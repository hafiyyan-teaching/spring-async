package manual.init.controller;

import manual.init.model.Customer;
import manual.init.repository.CustomerRepository;
import manual.init.utils.CustomerDBOperationSimulation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import java.util.concurrent.Future;

@Controller
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerDBOperationSimulation customerDBOperationSimulation;


    @GetMapping("/customer/index")
    public String index(Model model) {
        Iterable<Customer> allCustomer = customerRepository.findAll();
        model.addAttribute("allCustomer", allCustomer);
        return "listCustomer";
    }

    @GetMapping("/customer/update")
    public String updatePage(@RequestParam(name="id", required=true) Long id, Model model) {
        Customer aCustomer = customerRepository.findById(id).get();
        model.addAttribute("aCustomer", aCustomer);
        return "updateCustomer";
    }

    @PostMapping("/customer/update")
    public RedirectView updateLogic(@ModelAttribute Customer customer, Model model) {
        Future<Boolean> statusOfCreateOperation = customerDBOperationSimulation.saveCustomerDataAsynchronous(customer);
        return new RedirectView("/customer/index");
    }

    @GetMapping("/customer/create")
    public String createPage(Model model) {
        return "addCustomer";
    }

    @PostMapping("/customer/create")
    public RedirectView createLogic(@ModelAttribute Customer customer, Model model) {
        Future<Boolean> statusOfCreateOperation = customerDBOperationSimulation.saveCustomerDataAsynchronous(customer);
        return new RedirectView("/customer/index");
    }

    @GetMapping("/customer/delete")
    public RedirectView deleteLogic(@RequestParam(name="id", required=true) Long id, Model model) {
        Future<Boolean> statusOfDeleteOperation = customerDBOperationSimulation.deleteCustomerAsynchronous(id);
        return new RedirectView("/customer/index");
    }

}