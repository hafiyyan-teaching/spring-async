package manual.init.controller;

import manual.init.model.Movie;
import manual.init.utils.MovieDBOperationSimulation;
import manual.init.utils.OMDBAsynchronousCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
import org.apache.commons.collections4.CollectionUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;


@Controller
public class OMDBController {

    @Autowired
    private OMDBAsynchronousCall omdbAsynchronousCall;

    @Autowired
    private MovieDBOperationSimulation movieDBOperationSimulation;

    @GetMapping("/movie/index")
    public String listMovie(@RequestParam(name = "title", required = false, defaultValue = "Avenger") String title, Model model) throws IOException, ExecutionException, InterruptedException {

        CompletableFuture<List<Movie>> completableFutureMovieList = omdbAsynchronousCall.searchMovieByTitle(title);
        CompletableFuture<List<Movie>> completableFutureFavouriteMovieList = movieDBOperationSimulation.getAllMovie();

        CompletableFuture<Void> waitForBothFutureToBeCompleted = CompletableFuture.allOf(completableFutureMovieList, completableFutureFavouriteMovieList);

        List<Movie> movieFromApi = completableFutureMovieList.get();
        movieFromApi.removeAll(completableFutureFavouriteMovieList.get());

        model.addAttribute("listMovie", movieFromApi);
        model.addAttribute("title", title);
        return "listMovies";
    }


    @GetMapping("/movie/addFavorite")
    public RedirectView createLogic(@RequestParam(name = "id", required = true) String id, @RequestParam(name = "lastSearch", required = true) String lastSearch, Model model) {
        CompletableFuture<Movie> selectedMovie = omdbAsynchronousCall.searchMovieByIMDbId(id);
        selectedMovie.thenAccept(result -> {
            movieDBOperationSimulation.saveNewFavouriteMovie(result);
        });
        return new RedirectView("/movie/index?title="+lastSearch);
    }

    @GetMapping("/movie/removeFavorite")
    public RedirectView deleteLogic(@RequestParam(name = "id", required = true) Long id, Model model) {
        CompletableFuture<Boolean> selectedMovie = movieDBOperationSimulation.deleteFavouriteMovie(id);
        return new RedirectView("/movie/favorite");
    }

    @GetMapping("/movie/favorite")
    public String listFavoriteMovie(@RequestParam(name = "title", required = false, defaultValue = "") String title, Model model) throws IOException, ExecutionException, InterruptedException {

        CompletableFuture<List<Movie>> completableFutureFavouriteMovieList = movieDBOperationSimulation.getAllMovieByTitle(title);
        List<Movie> listFavoriteMovies = completableFutureFavouriteMovieList.get();
        System.out.println("Result of Searching " + title + ": " + listFavoriteMovies.size());
        model.addAttribute("listMovie", listFavoriteMovies);
        model.addAttribute("lastSearch", title);
        return "listFavoriteMovie";
    }

}
