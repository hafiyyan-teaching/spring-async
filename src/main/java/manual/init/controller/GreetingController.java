package manual.init.controller;

import manual.init.model.GreetingMessage;
import manual.init.model.GreetingResponseMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.util.HtmlUtils;

@Controller
public class GreetingController {

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public GreetingResponseMessage greeting(GreetingMessage message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new GreetingResponseMessage("Hello, " + HtmlUtils.htmlEscape(message.getName()) + " you say \""+ HtmlUtils.htmlEscape(message.getMessage()) +"\"!");
    }

    @GetMapping("/greeting/index")
    public String greetingIndex() {

        return "greeting";
    }

}