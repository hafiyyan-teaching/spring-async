package manual.init.repository;

import manual.init.model.Movie;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MovieRepository extends CrudRepository<Movie, Long> {

    List<Movie> findByTitle(String title);
    List<Movie> getAllByTitleContains(String title);
    List<Movie> findByImdbId(String id);

}
