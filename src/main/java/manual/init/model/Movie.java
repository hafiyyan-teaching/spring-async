package manual.init.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @SerializedName("Title")
    @Expose
    private String title;

    @SerializedName("Year")
    @Expose
    private String year;

    @SerializedName("imdbID")
    @Expose
    private String imdbId;

    @SerializedName("Type")
    @Expose
    private String type;

    @SerializedName("Poster")
    @Expose
    private String imageUrl;

    public Movie(String title, String year, String imdbId, String type, String imageUrl) {
        this.title = title;
        this.year = year;
        this.imdbId = imdbId;
        this.type = type;
        this.imageUrl = imageUrl;
    }

    public Movie(){
        this.title = "dummy";
        this.year = "dummy";
        this.imdbId = "dummy";
        this.type = "dummy";
        this.imageUrl = "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", imdbId='" + imdbId + '\'' +
                ", type='" + type + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj){
        boolean returnValue = false;
        try{
            Movie comparedMovieObj = (Movie) obj;
            returnValue = this.equalMovie(comparedMovieObj);
        } catch (Exception e){
            e.printStackTrace();
        }

        return returnValue;
    }

    private boolean equalMovie(Movie comparedMovie){
        if(this.imdbId.equalsIgnoreCase(comparedMovie.imdbId)){
            return true;
        } else{
            return false;
        }
    }
}
