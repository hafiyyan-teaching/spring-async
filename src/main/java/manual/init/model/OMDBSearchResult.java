package manual.init.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OMDBSearchResult {

    @SerializedName("Search")
    @Expose
    private List<Movie> retrievedMovie = new ArrayList<Movie>();
    private int totalResults;
    private boolean response;

    public List<Movie> getRetrievedMovie() {
        return retrievedMovie;
    }

    public void setRetrievedMovie(List<Movie> retrievedMovie) {
        this.retrievedMovie = retrievedMovie;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public boolean getResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "OMDBSearchResult{" +
                "totalResults=" + totalResults +
                ", response=" + response +
                '}';
    }
}
