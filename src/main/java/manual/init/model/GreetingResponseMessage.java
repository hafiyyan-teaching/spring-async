package manual.init.model;

public class GreetingResponseMessage {

    private String content;

    public GreetingResponseMessage(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
