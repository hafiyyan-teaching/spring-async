package manual.init.model;

public class GreetingMessage {

    private String name;
    private String message;

    public GreetingMessage() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
