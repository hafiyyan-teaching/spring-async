package manual.init.utils;

import manual.init.model.Movie;
import manual.init.model.OMDBSearchResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OMDBRetrofitService {

    @GET("/")
    Call<OMDBSearchResult> getMovieListFilteredByTitle(
            @Query("s") String searchKey, @Query("apiKey") String apiKey);

    @GET("/")
    Call<Movie> getMovieListFilteredByImdbId(
            @Query("i") String imdbId, @Query("apiKey") String apiKey);

}
