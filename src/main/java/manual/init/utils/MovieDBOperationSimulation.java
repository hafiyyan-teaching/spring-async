package manual.init.utils;

import manual.init.model.Movie;
import manual.init.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component("movieDBOperationSimulation")
public class MovieDBOperationSimulation {

    @Autowired
    private MovieRepository movieRepository;
    private ExecutorService executorService = Executors.newFixedThreadPool(10);

    public Future<Boolean> saveNewFavouriteMovie(Movie movie){
        return CompletableFuture.supplyAsync(() -> {
            try{
                movieRepository.save(movie);
                return true;
            } catch (Exception e){
                return false;
            }
        }, executorService);
    }

    public CompletableFuture<Boolean> deleteFavouriteMovie(long id){
        return CompletableFuture.supplyAsync(() -> {
            try{
                movieRepository.deleteById(id);
                return true;
            } catch (Exception e){
                return false;
            }
        }, executorService);
    }

    public CompletableFuture<List<Movie>> getAllMovie(){
        return CompletableFuture.supplyAsync(() -> {
            try{
                Iterable<Movie> result = movieRepository.findAll();
                return StreamSupport.stream(result.spliterator(), false).collect(Collectors.toList());

            } catch (Exception e){
                return new ArrayList<Movie>();
            }
        }, executorService);
    }

    public CompletableFuture<List<Movie>> getAllMovieByTitle(String title){
        return CompletableFuture.supplyAsync(() -> {
            try{
                Iterable<Movie> result = movieRepository.getAllByTitleContains(title);
                return StreamSupport.stream(result.spliterator(), false).collect(Collectors.toList());

            } catch (Exception e){
                return new ArrayList<Movie>();
            }
        }, executorService);
    }

}
