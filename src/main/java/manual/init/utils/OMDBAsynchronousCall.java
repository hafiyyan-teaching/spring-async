package manual.init.utils;

import manual.init.model.Movie;
import manual.init.model.OMDBSearchResult;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Component("omdbAsynchronousCall")
public class OMDBAsynchronousCall {

    private static final String omdbApiKey = "b682db52";
    private final OMDBRetrofitService httpClient = new Retrofit.Builder().baseUrl("http://www.omdbapi.com")
            .addConverterFactory(GsonConverterFactory.create()).build().create(OMDBRetrofitService.class);
    private ExecutorService executorService = Executors.newFixedThreadPool(10);

    public CompletableFuture<List<Movie>> searchMovieByTitle(String title){

        Call<OMDBSearchResult> apiFetchStatement = httpClient.getMovieListFilteredByTitle(title, omdbApiKey);
        return CompletableFuture.supplyAsync(() -> {
            List<Movie> fetchedMovieList;
            try {
                fetchedMovieList = apiFetchStatement.execute().body().getRetrievedMovie();
            } catch (IOException e) {
                e.printStackTrace();
                fetchedMovieList = new ArrayList<Movie>();
            }
            return fetchedMovieList;
        }, executorService);
    }

    public CompletableFuture<Movie> searchMovieByIMDbId(String id){

        Call<Movie> apiFetchStatement = httpClient.getMovieListFilteredByImdbId(id, omdbApiKey);
        return CompletableFuture.supplyAsync(() -> {
            Movie fetchedMovie;
            try {
                fetchedMovie = apiFetchStatement.execute().body();
            } catch (IOException e) {
                e.printStackTrace();
                fetchedMovie = new Movie();
            }
            return fetchedMovie;
        }, executorService);
    }

}
