package manual.init.utils;

import manual.init.model.Customer;
import manual.init.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Component("customerDBOperationSimulation")
public class CustomerDBOperationSimulation {

    @Autowired
    private CustomerRepository customerRepository;
    private ExecutorService executorService = Executors.newFixedThreadPool(10);

    public Future<Boolean> saveCustomerDataAsynchronous(Customer customer){
        return executorService.submit(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                try{
                    Thread.sleep(3000);
                    customerRepository.save(customer);
                    return true;
                } catch (Exception e){
                    return false;
                }
            }
        });
    }

    public Future<Boolean> deleteCustomerAsynchronous(long id){
        return executorService.submit(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                try{
                    Thread.sleep(3000);
                    customerRepository.deleteById(id);
                    return true;
                } catch (Exception e){
                    return false;
                }
            }
        });
    }

}
